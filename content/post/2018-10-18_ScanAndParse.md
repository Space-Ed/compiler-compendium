---
title: "Scanning and Parsing"
subtitle: "finding the structure in plain text"
tags: []
date: "2018-10-18"
---

The first step in compiling code is Scanning and Parsing, this is the process which takes
us from a sequence of input characters into a tree structured representation of our program.

We are able to describe parsers with the formal systems called grammars, a parser generally
is an implementation of a grammar.

$$
f(x) = \int_{-\infty}^\infty\hat f(\xi),e^{2 \pi i \xi x},d\xi
$$
